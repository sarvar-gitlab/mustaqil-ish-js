<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Laravel\Sanctum\PersonalAccessToken;
use Symfony\Component\HttpFoundation\Response;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required|string|max:255',
                'email' => 'required|email|unique:users,email',
                'password' => 'required|string|min:6|confirmed',
            ]);

            if ($validator->fails()) {
                return response([
                    'message' => $validator->errors(),
                ], Response::HTTP_BAD_REQUEST);
            }

            $user = User::create([
                'name' => $request->input('name'),
                'email' => $request->input('email'),
                'password' => Hash::make($request->input('password')),
            ]);

            $token = $user->createToken('token')->plainTextToken;

            return response([
                'token' => $token,
                'user' => $user,
            ]);
        } catch (\Exception $e) {
            return response([
                'message' => $e->getMessage(),
            ], Response::HTTP_BAD_REQUEST);
        }
    }


    public function login(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'email' => 'required|email',
                'password' => 'required|string',
            ]);

            if ($validator->fails()) {
                return response([
                    'message' => $validator->errors(),
                ], Response::HTTP_BAD_REQUEST);
            }

            if (!Auth::attempt($request->only('email', 'password'))) {
                throw new \Exception('Invalid credentials!');
            }

            /** @var \App\Models\MyUserModel $user **/
            $user = Auth::user();

            if (!$user) {
                throw new \Exception('User not found!');
            }

            $token = $user->createToken('token')->plainTextToken;

            return response([
                'token' => $token,
                'user' => $user,
            ]);
        } catch (\Exception $e) {
            return response([
                'message' => $e->getMessage(),
            ], Response::HTTP_UNAUTHORIZED);
        }
    }


    public function user()
    {
        try {
            $authenticatedUser = Auth::user();

            if (!$authenticatedUser) {
                throw new \Exception('User not authenticated.');
            }

            return $authenticatedUser;
        } catch (\Exception $e) {
            return response([
                'message' => $e->getMessage(),
            ], Response::HTTP_UNAUTHORIZED);
        }
    }


    public function logout()
    {
        try {
            auth()->user()->currentAccessToken()->delete();
            return response([
                'message' => 'success'
            ]);
        } catch (\Exception $e) {
            return response([
                'message' => $e->getMessage(),
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
}

}
