import { createContext, useContext, useState } from "react";
import axios from "../api/axios";
import { useNavigate } from "react-router-dom";

const AuthContext = createContext({});

export const AuthProvider = ({ children }) => {
  const [user, setUser] = useState(null);
  const [errors, setErrors] = useState([]);
  const navigate = useNavigate();

  const getUser = async () => {
    try {
      const authToken = localStorage.getItem("AuthToken");
      if (!authToken) return;

      const headers = {
        Authorization: `Bearer ${authToken}`,
      };

      await axios.get("/api/user", {
        headers: headers,
      });

      if (localStorage.getItem("user") && localStorage.getItem("AuthToken")) {
        setUser(JSON.parse(localStorage.getItem("user")));
      }
    } catch (e) {
      console.log(e);
      localStorage.removeItem("AuthToken");
      localStorage.removeItem("user");
    }
  };

  const setUserData = (userData) => {
    localStorage.setItem("AuthToken", userData.token);
    localStorage.setItem("user", JSON.stringify(userData.user));
    setUser(userData.user);
  };

  const login = async ({ ...data }) => {
    setErrors([]);
    try {
      const res = await axios.post("api/login", data);
      setUserData(res.data);
      navigate("/");
    } catch (e) {
      if (e.response && e.response.status === 400) {
        setErrors(e.response.data.message);
      } else if (e.response && e.response.status === 401) {
        setErrors(e.response.data);
      } else {
        console.error(e);
      }
    }
  };

  const register = async ({ ...data }) => {
    setErrors([]);
    try {
      const res = await axios.post("api/register", data);
      setUserData(res.data);
      navigate("/");
    } catch (e) {
      if (e.response && e.response.status === 400) {
        setErrors(e.response.data.message);
      } else {
        console.error(e);
      }
    }
  };

  const logout = () => {
    const authToken = localStorage.getItem("AuthToken");
    const headers = {
      Authorization: `Bearer ${authToken}`,
    };
    axios.post("/api/logout", null, { headers }).then(() => {
      setUser(null);
      localStorage.removeItem("AuthToken");
      localStorage.removeItem("user");
    });
  };

  return (
    <AuthContext.Provider
      value={{ user, errors, getUser, login, register, logout }}
    >
      {children}
    </AuthContext.Provider>
  );
};

export default function useAuthContext() {
  return useContext(AuthContext);
}
